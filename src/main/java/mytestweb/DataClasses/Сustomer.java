package mytestweb.DataClasses;

import mytestweb.DataClasses.Position;

public class Сustomer {
    private int id;
    private String surName;
    private String firstName;
    private String patronimicName;
    private Position positionCustomer;
    ;

    public Сustomer (int id,String surname,String firstName,String patronimicName, Position positionCustomer){
          this.id=id;
          this.surName=surname;
          this.firstName=firstName;
          this.patronimicName=patronimicName;
          this.positionCustomer=positionCustomer;
    }



    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronimicName() {
        return patronimicName;
    }

    public Position getPositionCustomer() {
        return positionCustomer;
    }

    public void setPositionCustomer(Position positionCustomer) {
        this.positionCustomer = positionCustomer;
    }

    public String getSurname() {
        return surName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setPatronimicName(String patronimicName) {
        this.patronimicName = patronimicName;
    }

    public void setSurname(String surname) {
        this.surName = surname;
    }
}
