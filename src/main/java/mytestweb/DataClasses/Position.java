package mytestweb.DataClasses;

public class Position {
    private int id;
    private String positionName;

  public   Position(int id, String positionName)
    {
        this.id=id;
        this.positionName=positionName;
    }

    public int getId() {
        return id;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
}
