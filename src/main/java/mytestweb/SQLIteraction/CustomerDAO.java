package mytestweb.SQLIteraction;

import mytestweb.DataClasses.Position;
import mytestweb.DataClasses.Сustomer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CustomerDAO {

    String databaseURL = "jdbc:mysql://localhost:3306/testproject?verifyServerCertificate=false&useSSL=false&requireSSL=false&useLegacyDatetimeCode=false&amp&serverTimezone=UTC";
    String user = "root";
    String password = "7578";

    public List<Сustomer> list() throws SQLException {
        List<Сustomer> listCustomer = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            String rt = (e.getMessage());
        }
        try (Connection connection = DriverManager.getConnection(databaseURL, user, password)) {
            String sql = "SELECT customer.id,customer.SurName, customer.FirstName, customer.PatronymicName,position.id, position.PositionName FROM customer left join position on customer.idPosition = position.id  order by customer.id asc ";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("customer.id");;
                String surname= result.getString("customer.SurName");
                String firstName= result.getString("customer.FirstName");;
                String patronimicName= result.getString("customer.PatronymicName");
                int positionID=result.getInt("position.id");
                String positionName= result.getString("position.PositionName");
                Position positionCustomer= new Position(positionID,positionName);
                Сustomer customer = new Сustomer(id, surname,firstName,patronimicName,positionCustomer);

                listCustomer.add(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }

        return listCustomer;
    }

}
