package mytestweb.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;

import mytestweb.SQLIteraction.CustomerDAO;
import mytestweb.DataClasses.Сustomer;

import java.util.List;

public class InitFormServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ListCustomer(req, resp, 1);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int customerId = Integer.parseInt(req.getParameter("name"));

        req.setAttribute("selectCustomerId", customerId);

        ListCustomer(req, resp, customerId);
    }

    protected void ListCustomer(HttpServletRequest req, HttpServletResponse resp, int selectCustomerID)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        CustomerDAO dao = new CustomerDAO();

        try {
            List<Сustomer> listCustomer = dao.list();

            String selectPositionName = listCustomer.get(selectCustomerID - 1).getPositionCustomer().getPositionName();
            String selectCustomerName = listCustomer.get(selectCustomerID - 1).getSurname() + " " + listCustomer.get(selectCustomerID - 1).getFirstName() + " " + listCustomer.get(selectCustomerID - 1).getPatronimicName();

            req.setAttribute("listCustomer", listCustomer);
            req.setAttribute("selectPositionName", selectPositionName);
            req.setAttribute("selectCustomerName", selectCustomerName);

            RequestDispatcher dispatcher = req.getRequestDispatcher("Index.jsp");
            dispatcher.forward(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

   /* void createViews(){
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/Style.css\"/>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Absense from work</title>");
        out.println("</head>");
        out.println("<body>");
        out.println(" <form action=\"InitForm\" method=\"post\">");
        out.println("<p><label for=\"name\">ФИО<em>*</em></label>");
        out.println("<select name=\"name\" id=\"name\" onchange=\"this.form.submit();\">");
        for (Сustomer customer : listCustomer) {
            out.println("<option value=" + customer.getId() + ">" + customer.getSurname() + " " + customer.getFirstName() + " " + customer.getPatronimicName() + "</option>");
        }
        out.println("</select></p>");
        out.println("</form>");


        out.println("<form class=\"AbsenceForm\" action=\"form\" method=\"post\">");
        out.println(" <fieldset>");
        out.println("<label for=\"name\">ФИО</label><input type=\"text\" id=\"name\" value=" + selectCustomerName + " name=\"name\"/></p>");
        out.println("<p><label for=\"position\">Должность</label><input type=\"text\" value=" + selectPositionName + " name=\"position\"");
        out.println(" id=\"position\"/></p>");
        out.println(" <p><label for=\"absenceDate\">Дата ухода и время ухода </label><input type=\"date\" name=\"absenceDate\"");
        out.println(" id=\"absenceDate\"></p>");
        out.println("<p><label for=\"uncomingTime\">Время отсутствия </label><input type=\"time\" name=\"outTime\" id=\"uncomingTime\"><span");
        out.println(" class=\"myte\">Укажите Час : Минуту </span> </p>");
        out.println("<label for=\"Cause\">Причина отклонения от графика</label>");
        out.println("<textarea name=\"сauseAbsense\" id=\"Cause\" rows=\"10\" cols=\"50\" maxlength=\"500\">");
        out.println("</textarea>");
        out.println("</fieldset>");
        out.println(" <p><input type=\"submit\" value=\"Отправить\"> <input type=\"reset\" value=\"Сбросить\"></p>");
        out.println("</body>");
        out.println("</html>");

    }*/
}
