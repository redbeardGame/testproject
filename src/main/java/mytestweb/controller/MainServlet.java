package mytestweb.controller;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

import java.io.IOException;
import java.io.FileWriter;

import mytestweb.SQLIteraction.CustomerDAO;
import mytestweb.DataClasses.Сustomer;

import java.util.List;

public class MainServlet extends HttpServlet {
     private String  lastValueOfName =  "lastValue";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ListCustomer(req, resp, 1);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String customerId = (req.getParameter("name"));

        req.setAttribute("selectCustomerId", customerId);

        String fulName = req.getParameter("name");

        String position = req.getParameter("position");
        String dateAbsence = req.getParameter("absenceDate");
        String uncomTime = req.getParameter("outTime");
        String causeAbsence = req.getParameter("сauseAbsense");


        if (fulName != null &&position !=null && dateAbsence != "" && uncomTime != "" && causeAbsence != "" ) {

            req.setAttribute("name", fulName);
            req.setAttribute("position", position);
            req.setAttribute("absenceDate", dateAbsence);
            req.setAttribute("uncomingTime", uncomTime);
            req.setAttribute("cause", causeAbsence);
            req.getRequestDispatcher("CustomerInfo.jsp").forward(req, resp);

            String insertQuere = "INSERT into absence (id_customer, absenceDate, absenceTime, AbsenceCause) VALUES (" + customerId + ", date ('" + dateAbsence + "'),time ('" + uncomTime + "'),'" + causeAbsence + "'); ";
            writeQuereInFile(insertQuere, "insertQuery.sql");
            try {
              //  Flyway flyway = new Flyway();
              //  flyway.setDataSource("jdbc:mysql://localhost:3306/testproject?verifyServerCertificate=false&useSSL=false&requireSSL=false&useLegacyDatetimeCode=false&amp&serverTimezone=UTC", "root", "7578");
              //  flyway.clean();
              //  flyway.migrate();

            } catch (FlywayException e) {
                String ex = (e.getMessage());
            }

        } else
            {

            //  ListCustomer(req, resp, customerId);
        }

    }


    protected void ListCustomer(HttpServletRequest req, HttpServletResponse resp, int selectCustomerID)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        CustomerDAO dao = new CustomerDAO();

        try {
            List<Сustomer> listCustomer = dao.list();

            String selectPositionName = listCustomer.get(selectCustomerID - 1).getPositionCustomer().getPositionName();
            String selectCustomerName = listCustomer.get(selectCustomerID - 1).getSurname() + " " + listCustomer.get(selectCustomerID - 1).getFirstName() + " " + listCustomer.get(selectCustomerID - 1).getPatronimicName();

            req.setAttribute("listCustomer", listCustomer);
            req.setAttribute("selectPositionName", selectPositionName);
            req.setAttribute("selectCustomerName", selectCustomerName);

            RequestDispatcher dispatcher = req.getRequestDispatcher("Index.jsp");
            dispatcher.forward(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    private void writeQuereInFile(String quere, String fileName) {
        try (FileWriter writer = new FileWriter(fileName, true)) {
            writer.write(quere);
            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            String rt = (e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testproject?verifyServerCertificate=false&useSSL=false&requireSSL=false&useLegacyDatetimeCode=false&amp&serverTimezone=UTC", "root", "7578");
            return dbConnection;
        } catch (SQLException e) {
            String rt = (e.getMessage());
        }
        return dbConnection;
    }


}
