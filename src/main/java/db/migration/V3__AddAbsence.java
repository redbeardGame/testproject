package db.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

public class V3__AddAbsence implements JdbcMigration {
    public void migrate(Connection connection) throws Exception {
        String query =readQueri("insertQuery.sql");
        PreparedStatement statement =
                connection.prepareStatement(query);

        try {
            statement.execute();
        } finally {
            statement.close();
        }
    }

    private  String readQueri(String fileName){
        try(FileReader reader = new FileReader(fileName))
        {


                int c;
                StringBuffer bufer = new StringBuffer();
                while ((c = reader.read()) != -1) {

                    bufer.append((char) c);
                }
                return bufer.toString();

        }
        catch(IOException ex){

            return (ex.getMessage());
        }
    }
}
