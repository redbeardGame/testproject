CREATE TABLE `testproject`.`position` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `PositionName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `Position_name` (`PositionName` ASC) VISIBLE);

CREATE TABLE `testproject`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `SurName` VARCHAR(30) NOT NULL,
  `FirstName` VARCHAR(30) NOT NULL,
  `PatronymicName` VARCHAR(30) NOT NULL,
  `idPosition` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `Customer_name` USING BTREE (`SurName`, `FirstName`, `PatronymicName`) VISIBLE,
  INDEX `Customer_position_fk_idx` (`idPosition` ASC) VISIBLE,
  CONSTRAINT `Customer_position_fk`
    FOREIGN KEY (`idPosition`)
    REFERENCES `testproject`.`position` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `testproject`.`absence` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_customer` INT NOT NULL,
  `absenceDate` DATE NOT NULL,
  `absenceTime` TIME NOT NULL,
  `AbsenceCause` VARCHAR(300) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `Absence_customer_idx` (`id_customer` ASC) VISIBLE,
  CONSTRAINT `Absence_customer`
    FOREIGN KEY (`id_customer`)
    REFERENCES `testproject`.`customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);